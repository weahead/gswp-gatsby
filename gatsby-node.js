const path = require(`path`)

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const postTemplate = path.resolve(`src/templates/post.js`)

  const result = await graphql(
    `
      query postsQuery {
        allWpPost {
          edges {
            node {
              databaseId
              slug
            }
          }
        }
      }
    `
  )

  result.data.allWpPost.edges.forEach(edge => {
    createPage({
      path: `${edge.node.slug}`,
      component: postTemplate,
      context: {
        databaseId: edge.node.databaseId,
      },
    })
  })
}
