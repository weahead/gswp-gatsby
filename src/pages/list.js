import React from "react"

import { Link, graphql } from "gatsby"

function List({ data }) {
  const {
    allWpPost: { nodes },
  } = data

  return (
    <ul>
      {nodes.map(node => {
        return (
          <li key={node.id}>
            <Link to={`/${node.slug}`}>{node.title}</Link>
          </li>
        )
      })}
    </ul>
  )
}

export default List

export const query = graphql`
  query {
    allWpPost {
      nodes {
        id
        slug
        title
      }
    }
  }
`
