import * as React from "react"
import { graphql } from "gatsby"

const IndexPage = ({ data }) => {
  return (
    <main style={{ maxWidth: "960px", margin: "auto" }}>
      {data.allWpPost.nodes.map(node => {
        return (
          <article
            key={node.id}
            style={{
              border: "solid 1px #000",
              margin: "50px",
              padding: "20px",
              background: "#cccc",
            }}
          >
            <header>
              <h2>{node.title}</h2>
            </header>
            <div dangerouslySetInnerHTML={{ __html: node.content }}></div>
          </article>
        )
      })}
    </main>
  )
}

export default IndexPage

export const query = graphql`
  {
    allWpPost(limit: 10) {
      pageInfo {
        totalCount
      }
      nodes {
        id
        title
        content
        contentJson
      }
    }
  }
`
