import React from "react"
import { graphql } from "gatsby"

function PostTemplate({ data }) {
  const {
    wpPost: { title, content, contentJson },
  } = data

  return (
    <article
      style={{
        border: "solid 1px #000",
        margin: "50px",
        padding: "20px",
        background: "#cccc",
      }}
    >
      <header>
        <h2>{title}</h2>
      </header>
      <div dangerouslySetInnerHTML={{ __html: content }}></div>
      <pre>{JSON.stringify(contentJson, null, 2)}</pre>
    </article>
  )
}

export default PostTemplate

export const query = graphql`
  query Post($databaseId: Int) {
    wpPost(databaseId: { eq: $databaseId }) {
      title
      content
      contentJson
    }
  }
`
