import { GatsbyFunctionRequest, GatsbyFunctionResponse } from "gatsby"
export default function handler(
  req: GatsbyFunctionRequest,
  res: GatsbyFunctionResponse
) {
  res.setHeader("Cache-Control", [
    "max-age=60",
    "s-maxage=60",
    "stale-while-revalidate=60",
  ])
  res.setHeader("x-robots-tag", "all")
  res.send(`I am TYPESCRIPT: ${new Date().toString()}`)
}
