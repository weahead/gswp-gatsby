import { GatsbyFunctionRequest, GatsbyFunctionResponse } from "gatsby"
export default function handler(
  req: GatsbyFunctionRequest,
  res: GatsbyFunctionResponse
) {
  res.setHeader(`Cache-Control`, [
    `max-age=10`,
    `s-maxage=20`,
    `stale-while-revalidate=10`,
  ])

  let msg = process.env.MESSAGE

  res.send(`I am ${msg}: ${new Date().toString()}`)
}
